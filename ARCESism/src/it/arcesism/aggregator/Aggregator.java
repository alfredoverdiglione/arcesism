package it.arcesism.aggregator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import sofia_kp.KPICore;
import sofia_kp.SIBResponse;

public class Aggregator {

	private PGAUpdateHandler pgaUpdateHandler;
	private BoolUpdateHandler boolUpdateHandler;

	int SIBPort = 10010;
	String SIB_host = "127.0.0.1";
	String SIBName = "X";

	private SIBResponse resp = null;
	private KPICore kp;

	private String sub_id_PGA = null;
	private String sub_id_Boolean = null;

	public Aggregator(String sib_host2, int sib_port) {
		this.SIB_host = sib_host2;
		this.SIBPort = sib_port;
		
	}

	// Method to Join the SIB
	public void join() {
		pgaUpdateHandler = new PGAUpdateHandler(SIB_host, SIBPort, SIBName);
		boolUpdateHandler = new BoolUpdateHandler(SIB_host, SIBPort, SIBName);

		kp = new KPICore(SIB_host, SIBPort, SIBName);
		resp = kp.join();

		if (!resp.isConfirmed()) {
			System.err.println("Error joining the SIB");
			System.exit(0);
		} else {
			System.out.println("Joined!");
		}
			
		System.out.println("Initiating PGA subscription");
		String pattern_PGA = "SELECT ?s ?p ?o  WHERE {   ?s ?p ?o . ?s <rdf:type> \"PGASensorData\" }";
		resp = kp.subscribeSPARQL(pattern_PGA, pgaUpdateHandler);
		sub_id_PGA = resp.subscription_id;

		System.out.println("Initiating Boolean subscription");
		String pattern_Boolean = "SELECT ?s ?p ?o WHERE {   ?s ?p ?o . ?s <rdf:type> \"BooleanSensorData\" }";
		resp = kp.subscribeSPARQL(pattern_Boolean, boolUpdateHandler);
		sub_id_Boolean = resp.subscription_id;

	}

	//daemon that reads from stdin
	private void run() {
		BufferedReader bis = new BufferedReader(new InputStreamReader(System.in));
		String read = null;
	
		while (true) {
			System.out.println("Type q to quit");
			try {
				read = bis.readLine();
			} catch (IOException e) {
				e.printStackTrace();
				System.exit(-1);
			}
			if (read == null){
				quit();
			} else {
				read.trim();
				if (read.equalsIgnoreCase("q"))
					quit();
			}
		}
	}

	//method to close subscription before shutting down
	private void quit() {
		System.out.println("Terminating PGA subscription");
		resp = kp.unsubscribe(sub_id_PGA);
		System.out.println("Terminating Boolean subscription");
		resp = kp.unsubscribe(sub_id_Boolean);
		System.out.println("Leaving SIB");
		kp.leave();
		pgaUpdateHandler.close();
		boolUpdateHandler.close();
		System.exit(0);
	}
	
	public static final String usage = "Aggregator - usage:\njava -jar this_jar sib_host sib_port\nor just java -jar this_jar -a to autoconfig it for local operation";

	public static void main(String[] args) {
		String sib_host = "";
		int sib_port = 0;
		boolean solved = false;

		try {
			if (args.length == 1 && args[0].equals("-a")) {
				sib_host = "localhost";
				sib_port = 10010;
				solved = true;
			} else if (args.length == 2) {
				sib_host = args[0];
				sib_port = Integer.parseInt(args[1]);
				solved = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (!solved) {
				System.out.println(usage);
				System.exit(99);
			}
		}

		Aggregator gator = new Aggregator(sib_host, sib_port);

		// join smart space
		gator.join();

		// wait for exit request
		gator.run();
	}

}
