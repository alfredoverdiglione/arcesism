package it.arcesism.aggregator;

import sofia_kp.KPICore;
import sofia_kp.SIBResponse;
import sofia_kp.SSAP_sparql_response;
import sofia_kp.iKPIC_subscribeHandler2;
import java.util.HashMap;
import java.util.Vector;

import it.arcesism.Constants;

public class BoolUpdateHandler implements iKPIC_subscribeHandler2 {

	private KPICore kp;
	private SIBResponse resp = null;

	public void kpic_RDFEventHandler(Vector<Vector<String>> newTriples, Vector<Vector<String>> oldTriples,
			String indSequence, String subID) {

	}

	public void kpic_SPARQLEventHandler(SSAP_sparql_response newResults, SSAP_sparql_response oldResults,
			String indSequence, String subID) {
		Vector<String[]> row = null;
		HashMap<String, Long> NewData = new HashMap<String, Long>();
		String currentRoom = null;
		String currentSensor = null;
		while (newResults != null && newResults.hasNext()) {
			newResults.next();

			row = newResults.getRow();

			// se riga con timestamp, inserisci dati nella mappa
			if (row.get(1)[2].contains("HasTimeStamp")) {
				NewData.put(row.get(0)[2], Long.parseLong(row.get(2)[2]));
			}
			// se riga con sensore, salva il suo nome
			if (row.get(1)[2].contains("IsProducedBy")) {
				currentSensor = row.get(2)[2];
			}
		}

		// Find Room
		{
			String query = "SELECT ?r WHERE {<" + currentSensor + "> <" + Constants.ns + "IsInRoom> ?r.}";
			resp = kp.querySPARQL(query);
			Vector<String[]> riga = null;
			SSAP_sparql_response sqr = resp.sparqlquery_results;
			sqr.next();
			riga = sqr.getRow();
			currentRoom = riga.get(0)[2];	
		}

		for (String uri : NewData.keySet()) {
			String query = "SELECT ?s ?o ?sensor WHERE {?s  <rdf:type> \"PGASensorData\" . ?s <" + Constants.ns
					+ "HasTimeStamp> ?o . ?s <" + Constants.ns + "IsProducedBy> ?sensor." + "?sensor <" + Constants.ns
					+ "IsInRoom> <" + currentRoom + "> .} ";
			resp = kp.querySPARQL(query);

			Vector<String[]> riga = null;
			SSAP_sparql_response sqr = resp.sparqlquery_results;

			while (sqr != null && sqr.hasNext()) {
				sqr.next();
				riga = sqr.getRow();

				long millis = Long.parseLong(riga.get(1)[2]);
				long diff = Math.abs(millis - NewData.get(uri));
				if (diff < Constants.doorQuakeInterferenceThreshold) {
					System.out.println("Setting tuple " + uri + " to FALSE POSITIVE");
					// siamo in fascia di aggiornamento
					resp = kp.update(riga.get(0)[2], Constants.ns + "IsFakePositive", "true", "uri", "literal",
							riga.get(0)[2], Constants.ns + "IsFakePositive", "false", "uri", "literal");
				}

				// cancellare le tuple vecchie
				long timediff = NewData.get(uri) - millis;
				if (timediff > Constants.timeBeforePGAStale) {
					System.out.println("Removing outdated PGA  tuple (ts: " + riga.get(0)[2] + ")");
					resp = kp.remove(riga.get(0)[2], "http://www.nokia.com/NRC/M3/sib#any",
							"http://www.nokia.com/NRC/M3/sib#any", "uri", "uri");

				}

			}

		}

	}

	public void close() {
		kp.leave();
	}

	public void kpic_UnsubscribeEventHandler(String sub_ID) {

	}

	public void kpic_ExceptionEventHandler(Throwable SocketException) {

	}

	public BoolUpdateHandler(String SIB_host, int SIBPort, String SIBName) {
		kp = new KPICore(SIB_host, SIBPort, SIBName);
		resp = kp.join();

		if (!resp.isConfirmed()) {
			System.err.println("Error joining the SIB");
			System.exit(0);
		} else {
			System.out.println("Joined!");
		}

	}
}
