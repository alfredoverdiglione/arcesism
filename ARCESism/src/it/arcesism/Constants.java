package it.arcesism;

import java.text.SimpleDateFormat;

public class Constants {
	// Namespace
	public static final String ns = "http://www.arces.unibo.it/ARCESism#";
	// Utility date formatter
	public static SimpleDateFormat sfd = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	// Time before declaring data stale and removing corresponding tuples from DB
	public static final long timeBeforePGAStale = 24*60*60*1000;
	public static final long timeBeforeBoolStale = 30000;
	
	// Temporal distance between a Quake and a door slam to justify an interference
	public static final long doorQuakeInterferenceThreshold = 30000;
	
}
