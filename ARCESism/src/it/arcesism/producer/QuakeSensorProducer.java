package it.arcesism.producer;

import it.arcesism.Constants;
import it.arcesism.gyroscope.Gyroscope;
import it.arcesism.gyroscope.MPU6050;
import it.arcesism.gyroscope.MockGyroscope;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.Vector;

public class QuakeSensorProducer extends SensorProducer {
	// A specific producer that is attached to a Gyroscope and detects peak
	// ground acceleration

	// Time interval between two updates on the SIB in milliseconds
	private static final int updateInterval = 100;
	// Time interval between two readings of the sensor in milliseconds
	private static final int inputInterval = 100;

	private String unityOfMeasureURI;
	private Gyroscope sensor;
	private float horizontalPGA, verticalPGA;
	private final Object lock = new Object();
	private Thread sensorThread, sibThread;
	private boolean runthread = true;

	public QuakeSensorProducer(String sensorURI, String measurandURI, String unityOfMeasureURI, Gyroscope sensor,
			String sib_host, int sib_port) {
		this.SIB_host = sib_host;
		this.SIBPort = sib_port;
		this.sensorURI = sensorURI;
		this.measurandURI = measurandURI;
		this.unityOfMeasureURI = unityOfMeasureURI;
		this.sensor = sensor;

	}

	// Method to regularly produce data to be uploaded to the SIB
	@Override
	public void run() {
		// Define thread that reads value from the sensor
		sensorThread = new Thread() {
			public void run() {
				// forever
				while (runthread) {
					// compute horizontal ground acceleration
					float horizontal = (float) Math.sqrt(sensor.getXAcceleration() * sensor.getXAcceleration()
							+ sensor.getZAcceleration() * sensor.getZAcceleration());
					// compute vertical ground acceleration
					float vertical = Math.abs(sensor.getYAcceleration());
					System.out.println("Read from sensor: vert: " + vertical + " hor: " + horizontal);
					// update values
					synchronized (lock) {
						if (horizontalPGA < horizontal)
							horizontalPGA = horizontal;
						if (verticalPGA < vertical)
							verticalPGA = vertical;
					}

					// sleep
					try {
						Thread.sleep(inputInterval);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		};

		// Define thread that inserts new values on the SIB
		sibThread = new Thread() {
			public void run() {
				// forever
				while (runthread) {
					// read and clear values
					float horizontal, vertical;
					synchronized (lock) {
						horizontal = horizontalPGA;
						horizontalPGA = 0;
						vertical = verticalPGA;
						verticalPGA = 0;
					}

					// insert values on SIB
					generateInstance(horizontal, vertical, new Date());

					// sleep
					try {
						Thread.sleep(updateInterval);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		};

		// run threads
		sensorThread.start();
		sibThread.start();

		// wait for user input
		BufferedReader bis = new BufferedReader(new InputStreamReader(System.in));
		String read = null;

		while (true) {
			System.out.println("Type q to quit");
			try {
				read = bis.readLine();
			} catch (IOException e) {
				e.printStackTrace();
				System.exit(-1);
			}
			if (read == null){
				quit();
			} else {
				read.trim();
				if (read.equalsIgnoreCase("q"))
					quit();
			}
		}
	}

	// Method to upload data on the SIB
	public void generateInstance(float horizontal, float vertical, Date timestamp) {
		Vector<Vector<String>> triples = new Vector<Vector<String>>();

		Vector<String> triple1 = new Vector<String>();
		Vector<String> triple2 = new Vector<String>();
		Vector<String> triple3 = new Vector<String>();
		Vector<String> triple4 = new Vector<String>();
		Vector<String> triple5 = new Vector<String>();
		Vector<String> triple6 = new Vector<String>();
		Vector<String> triple7 = new Vector<String>();
		Vector<String> triple8 = new Vector<String>();

		String subject = Constants.ns + "PGASensorData_" + new Date().getTime();
		String predicate = Constants.ns + "IsProducedBy";
		String object = Constants.ns + "" + sensorURI;

		triple1.add(subject);
		triple1.add(predicate);
		triple1.add(object);
		triple1.add("uri");
		triple1.add("uri");

		predicate = Constants.ns + "HasUoM";
		object = Constants.ns + "" + unityOfMeasureURI;

		triple2.add(subject);
		triple2.add(predicate);
		triple2.add(object);
		triple2.add("uri");
		triple2.add("uri");

		predicate = Constants.ns + "HasMeasurand";
		object = Constants.ns + "" + measurandURI;

		triple3.add(subject);
		triple3.add(predicate);
		triple3.add(object);
		triple3.add("uri");
		triple3.add("uri");

		predicate = Constants.ns + "HasPGASensorDataHorizontalValue";
		object = "" + horizontal;

		triple4.add(subject);
		triple4.add(predicate);
		triple4.add(object);
		triple4.add("uri");
		triple4.add("literal");

		predicate = Constants.ns + "HasPGASensorDataVerticalValue";
		object = "" + vertical;
		
		triple5.add(subject);
		triple5.add(predicate);
		triple5.add(object);
		triple5.add("uri");
		triple5.add("literal");

		predicate = Constants.ns + "HasTimeStamp";
		object = "" + timestamp.getTime();
		triple6.add(subject);
		triple6.add(predicate);
		triple6.add(object);
		triple6.add("uri");
		triple6.add("literal");

		predicate = Constants.ns + "IsFakePositive";
		object = "false";
		triple7.add(subject);
		triple7.add(predicate);
		triple7.add(object);
		triple7.add("uri");
		triple7.add("literal");

		triple8.add(subject);
		triple8.add("rdf:type");
		triple8.add("PGASensorData");
		triple8.add("uri");
		triple8.add("literal");

		triples.add(triple1);
		triples.add(triple2);
		triples.add(triple3);
		triples.add(triple4);
		triples.add(triple5);
		triples.add(triple6);
		triples.add(triple7);
		triples.add(triple8);

		resp = kp.insert(triples);
		if (resp.isConfirmed()) {
			Date d = new Date();
			System.out.println(Constants.sfd.format(d) + "> Insert successful!");
		} else {
			System.out.println("Insert failed!");
		}

	}

	/** Define this values according to ontology */
	public static String MySensorURIprefix = "QuakeSensor_";
	public static String MyMeasurandURI = "Peak_Ground_Acceleration";
	public static String MyUnityOfMeasureURI = "Meters_per_Second_Squared";
	public static final String usage = "QuakeSensorProducer - usage:\n"
			+ "java -jar this_jar sib_host sib_port sensor_number\n"
			+ "or just java -jar this_jar -a to autoconfig it for local operation on sensor 1";

	private void quit() {
		System.out.println("Stopping Threads");
		runthread = false;
		System.out.println("Leaving SIB");
		kp.leave();
		System.exit(0);
	}
	
	
	public static void main(String[] args) {
		String sib_host = "", sensor_suffix = "";
		int sib_port = 0;
		boolean solved = false;

		try {
			if (args.length == 1 && args[0].equals("-a")) {
				sib_host = "localhost";
				sib_port = 10010;
				sensor_suffix = "1";
				solved = true;
			} else if (args.length == 3) {
				sib_host = args[0];
				sib_port = Integer.parseInt(args[1]);
				sensor_suffix = args[2];
				solved = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (!solved) {
				System.out.println(usage);
				System.exit(99);
			}
		}

		// QuakeSensorProducer sensor = new QuakeSensorProducer(MySensorURI,
		// MyMeasurandURI, MyUnityOfMeasureURI, MPU6050.GetInstance());
		QuakeSensorProducer sensor = new QuakeSensorProducer(MySensorURIprefix + sensor_suffix, MyMeasurandURI,
				MyUnityOfMeasureURI, MPU6050.GetInstance(), sib_host, sib_port);

		// Initialise gyroscope
		sensor.sensor.calibrate();

		// Join smart space
		sensor.join();

		// start daemon
		sensor.run();
	}
}
