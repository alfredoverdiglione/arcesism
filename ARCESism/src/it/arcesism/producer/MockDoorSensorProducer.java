package it.arcesism.producer;

import it.arcesism.doorsensor.DoorSensor;
import it.arcesism.doorsensor.MockDoorSensor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;

public class MockDoorSensorProducer extends DoorSensorProducer {


	public MockDoorSensorProducer(String sensorURI, String measurandURI, DoorSensor sensor, String sib_host,
			int sib_port) {
		super(sensorURI, measurandURI, sensor, sib_host, sib_port);
	}

	@Override
	public void run() {

		// wait for user input
		BufferedReader bis = new BufferedReader(new InputStreamReader(System.in));
		String read = null;
		
		while (true) {
			System.out.println("Type q to quit, s to cause a door slam");
			try {
				read = bis.readLine();
			} catch (IOException e) {
				e.printStackTrace();
				System.exit(-1);
			}
			if (read == null) {
				quit();
			} else {
				read.trim();
				if (read.equalsIgnoreCase("q"))
					quit();
				else if (read.equalsIgnoreCase("s"))
					GenerateInstance(true, new Date());
			}
		}
	}

	public static void main(String[] args) {
		String sib_host = "", sensor_suffix = "";
		int sib_port = 0;
		boolean solved = false;

		try {
			if (args.length == 1 && args[0].equals("-a")) {
				sib_host = "localhost";
				sib_port = 10010;
				sensor_suffix = "1";
				solved = true;
			} else if (args.length == 3) {
				sib_host = args[0];
				sib_port = Integer.parseInt(args[1]);
				sensor_suffix = args[2];
				solved = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (!solved) {
				System.out.println(usage);
				System.exit(99);
			}
		}

		MockDoorSensorProducer sensor = new MockDoorSensorProducer(MySensorURIprefix + sensor_suffix, MyMeasurandURI,
				new MockDoorSensor(), sib_host, sib_port);

		// Join smart space
		sensor.join();

		// start daemon
		sensor.run();
	}
}
