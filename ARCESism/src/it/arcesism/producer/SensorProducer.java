package it.arcesism.producer;

import sofia_kp.KPICore;
import sofia_kp.SIBResponse;

public abstract class SensorProducer {
    //A generic sensor producing data for the System

    //URIs for this object within the SIB
    protected String sensorURI, measurandURI;
    protected int SIBPort = 10010;	
	protected String SIB_host = "127.0.0.1";
	protected String SIBName = "X";
	
	protected SIBResponse resp = null;
    protected KPICore kp;

    //Method to Join the SIB
    public void join() {
    	kp = new KPICore(SIB_host, SIBPort, SIBName);
        resp = kp.join();
        
        if(!resp.isConfirmed()){
        	System.err.println("Error joining the SIB");
        	System.exit(0);
        }else{
        	System.out.println("Joined!");
        }
    }

    //Method to disjoin SIB
    public void close(){
    	kp.leave();
    }
    
    //Method to regularly produce data to be uploaded to the SIB
    public abstract void run();
}
