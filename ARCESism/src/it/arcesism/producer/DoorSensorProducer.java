package it.arcesism.producer;

import it.arcesism.Constants;
import it.arcesism.doorsensor.DoorSensor;
import it.arcesism.doorsensor.MockDoorSensor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.Vector;

public class DoorSensorProducer extends SensorProducer {
	// A specific producer that is attached to a DoorSensor and detects doors
	// opening

	// Time interval between two updates on the SIB in milliseconds
	private static final int updateInterval = 15000;
	private DoorSensor sensor;
	protected boolean runthread = true;

	public DoorSensorProducer(String sensorURI, String measurandURI, DoorSensor sensor, String sib_host, int sib_port) {
		this.SIB_host = sib_host;
		this.SIBPort = sib_port;
		this.sensorURI = sensorURI;
		this.measurandURI = measurandURI;
		this.sensor = sensor;
	}

	@Override
	public void run() {
		Thread updateThread = new Thread() {
			public void run() {
				while (runthread) {

					// read and insert values on SIB
					GenerateInstance(sensor.getDoorSlam(), new Date());

					// sleep
					try {
						Thread.sleep(updateInterval);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		};

		// run thread
		updateThread.start();

		// wait for user input
		BufferedReader bis = new BufferedReader(new InputStreamReader(System.in));
		String read = null;
		
		while (true) {
			System.out.println("Type q to quit");
			try {
				read = bis.readLine();
			} catch (IOException e) {
				e.printStackTrace();
				System.exit(-1);
			}
			if (read == null) {
				quit();
			} else {
				read.trim();
				if (read.equalsIgnoreCase("q"))
					quit();
			}
		}
	}

	// Method to upload data on the SIB
	public void GenerateInstance(boolean value, Date timestamp) {

		if (!value)
			return;
		Vector<Vector<String>> triples = new Vector<Vector<String>>();

		Vector<String> triple1 = new Vector<String>();
		Vector<String> triple2 = new Vector<String>();
		Vector<String> triple3 = new Vector<String>();
		Vector<String> triple4 = new Vector<String>();
		Vector<String> triple5 = new Vector<String>();

		String subject = Constants.ns + "BooleanSensorData_" + new Date().getTime();
		String predicate = Constants.ns + "IsProducedBy";
		String object = Constants.ns + "" + sensorURI;

		triple1.add(subject);
		triple1.add(predicate);
		triple1.add(object);
		triple1.add("uri");
		triple1.add("uri");

		predicate = Constants.ns + "HasMeasurand";
		object = Constants.ns + "" + measurandURI;

		triple2.add(subject);
		triple2.add(predicate);
		triple2.add(object);
		triple2.add("uri");
		triple2.add("uri");

		predicate = Constants.ns + "HasBoolSensorDataValue";
		object = "" + value;

		triple3.add(subject);
		triple3.add(predicate);
		triple3.add(object);
		triple3.add("uri");
		triple3.add("literal");

		predicate = Constants.ns + "HasTimeStamp";
		object = "" + timestamp.getTime();

		triple4.add(subject);
		triple4.add(predicate);
		triple4.add(object);
		triple4.add("uri");
		triple4.add("literal");

		triple5.add(subject);
		triple5.add("rdf:type");
		triple5.add("BooleanSensorData");
		triple5.add("uri");
		triple5.add("literal");

		triples.add(triple1);
		triples.add(triple2);
		triples.add(triple3);
		triples.add(triple4);
		triples.add(triple5);

		resp = kp.insert(triples);

		if (resp.isConfirmed()) {
			Date d = new Date();
			System.out.println(Constants.sfd.format(d) + "> Insert successful!");
		} else {
			System.out.println("Insert failed!");
		}

		/*
		 * + fare upload dei due dati sulla SIB facendo una insert
		 */
	}

	/** Define this values according to ontology */
	public static final String MySensorURIprefix = "DoorSensor_";
	public static final String MyMeasurandURI = "Door_Slammed";
	public static final String usage = "DoorSensorProducer - usage:\n"
			+ "java -jar this_jar sib_host sib_port sensor_number\n"
			+ "or just java -jar this_jar -a to autoconfig it for local operation on sensor 1";

	protected void quit() {
		System.out.println("Stopping Threads");
		runthread = false;
		System.out.println("Leaving SIB");
		kp.leave();
		System.exit(0);
	}

	public static void main(String[] args) {
		String sib_host = "", sensor_suffix = "";
		int sib_port = 0;
		boolean solved = false;

		try {
			if (args.length == 1 && args[0].equals("-a")) {
				sib_host = "localhost";
				sib_port = 10010;
				sensor_suffix = "1";
				solved = true;
			} else if (args.length == 3) {
				sib_host = args[0];
				sib_port = Integer.parseInt(args[1]);
				sensor_suffix = args[2];
				solved = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (!solved) {
				System.out.println(usage);
				System.exit(99);
			}
		}

		DoorSensorProducer sensor = new DoorSensorProducer(MySensorURIprefix + sensor_suffix, MyMeasurandURI,
				new MockDoorSensor(), sib_host, sib_port);

		// Initialise door sensor;
		sensor.sensor.calibrate();

		// Join smart space
		sensor.join();

		// start daemon
		sensor.run();
	}
}
