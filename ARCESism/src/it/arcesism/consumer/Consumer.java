package it.arcesism.consumer;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.Vector;

import it.arcesism.Constants;
import sofia_kp.KPICore;
import sofia_kp.SIBResponse;
import sofia_kp.SSAP_sparql_response;
import sofia_kp.iKPIC_subscribeHandler2;

public class Consumer implements iKPIC_subscribeHandler2 {

	int SIBPort = 10010;
	String SIB_host = "127.0.0.1";
	String SIBName = "X";
	String Room;
	private SIBResponse resp = null;
	private KPICore kp;

	private String sub_id = null;

	public Consumer(String sib_host2, int sib_port, String cfp, String room) {
		SIBPort = sib_port;
		SIB_host = sib_host2;
		communicationFilePath = cfp;
		Room = room;
	}

	// Method to Join the SIB
	public void join() {

		kp = new KPICore(SIB_host, SIBPort, SIBName);
		resp = kp.join();

		if (!resp.isConfirmed()) {
			System.err.println("Error joining the SIB");
			System.exit(0);
		} else {
			System.out.println("Joined!");
		}

		String pattern = "SELECT ?s ?ts ?hv ?vv ?valid ?sensor WHERE {"
				+ "?s <rdf:type> \"PGASensorData\" . ?s <" + Constants.ns + "HasTimeStamp> ?ts ."
				+ "?s <" + Constants.ns + "HasPGASensorDataHorizontalValue> ?hv ."
				+ "?s <" + Constants.ns + "HasPGASensorDataVerticalValue> ?vv ."
				+ "?s <" + Constants.ns + "IsFakePositive> ?valid ."
				+ "?s <" + Constants.ns + "IsProducedBy> ?sensor."
				+ "?sensor <" + Constants.ns + "IsInRoom> <" + Constants.ns + Room + "> .}";
		resp = kp.subscribeSPARQL(pattern, this);
		sub_id = resp.subscription_id;

	}

	// daemon method
	public void run() {
		BufferedReader bis = new BufferedReader(new InputStreamReader(System.in));
		String read = null;

		while (true) {
			System.out.println("Type q to quit");
			try {
				read = bis.readLine();
			} catch (IOException e) {
				e.printStackTrace();
				System.exit(-1);
			}
			if (read == null) {
				quit();
			} else {
				read.trim();
				if (read.equalsIgnoreCase("q"))
					quit();
			}
		}
	}

	// method to close subscription before shutting down
	private void quit() {
		System.out.println("Terminating subscription");
		resp = kp.unsubscribe(sub_id);
		System.out.println("Leaving SIB");
		kp.leave();
		System.exit(0);
	}

	private static String communicationFilePath = "./output.txt";
	private HashMap<Date, Object[]> memoryMap = new HashMap<Date, Object[]>();
	private SimpleDateFormat dateForma = new SimpleDateFormat("yy-MM-dd HH:mm:ss.SSS");

	private void UpdateFile() {
		try {
			PrintWriter pw = new PrintWriter(new FileWriter(communicationFilePath, false));
			System.out.println("Aggiornato file " + communicationFilePath);
			SortedSet<Date> sortedKeys = new TreeSet<>(memoryMap.keySet());
			for (Date d : sortedKeys) {
				Object[] vals = memoryMap.get(d);
				pw.println(String.format(java.util.Locale.US, "%.2f", (Float) (vals[0])) + ";"
						+ String.format(java.util.Locale.US, "%.2f", (Float) (vals[1])) + ";" + (Boolean) (vals[2])
						+ ";" + dateForma.format(d));
			}
			pw.close();
		} catch (IOException e) {
			System.exit(999);
		}
	}

	public void showValue(Date timestamp, float horizontalPGA, float verticalPGA) {
		// Update memory map
		memoryMap.put(timestamp, new Object[] { horizontalPGA, verticalPGA, true });

		// Update file
		UpdateFile();
	}

	public void invalidateValue(Date timestamp) {
		// Update memory map
		Object[] vals = memoryMap.get(timestamp);
		vals[2] = false;
		memoryMap.put(timestamp, vals);

		// Update file
		UpdateFile();
	}

	public void kpic_RDFEventHandler(Vector<Vector<String>> newTriples, Vector<Vector<String>> oldTriples,
			String indSequence, String subID) {

	}

	public void kpic_SPARQLEventHandler(SSAP_sparql_response newResults, SSAP_sparql_response oldResults,
			String indSequence, String subID) {
		System.out.println("ricevuta tripla");
		Vector<String[]> row = null;

		while (newResults != null && newResults.hasNext()) {
			newResults.next();
			row = newResults.getRow();

			for (String[] r : row) {
				for (String s : r)
					System.out.print(s + " ");
				System.out.println();
			}
			showValue(new Date(Long.parseLong(row.get(1)[2])), Float.parseFloat(row.get(2)[2]),
					Float.parseFloat(row.get(3)[2]));
			if (Boolean.parseBoolean(row.get(4)[2]))
				invalidateValue(new Date(Long.parseLong(row.get(1)[2])));

		}
	}

	public void kpic_UnsubscribeEventHandler(String sub_ID) {

	}

	public void kpic_ExceptionEventHandler(Throwable SocketException) {

	}

	public static final String usage = "Consumer - usage:\njava -jar this_jar sib_host sib_port room_number [communication_file_path]\nor just java -jar this_jar -a to autoconfig it for local operation in room 1 and ./output.txt as communication file path";

	public static void main(String[] args) {
		String sib_host = "", cfp = "./output.txt";
		int roomno = 1;
		int sib_port = 0;
		boolean solved = false;

		try {
			if (args.length == 1 && args[0].equals("-a")) {
				sib_host = "localhost";
				sib_port = 10010;
				solved = true;
			} else if (args.length == 3) {
				sib_host = args[0];
				sib_port = Integer.parseInt(args[1]);
				roomno = Integer.parseInt(args[2]);
				solved = true;
			} else if (args.length == 4) {
				sib_host = args[0];
				sib_port = Integer.parseInt(args[1]);
				roomno = Integer.parseInt(args[2]);
				cfp = args[3];
				solved = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (!solved) {
				System.out.println(usage);
				System.exit(99);
			}
		}

		Consumer consumer = new Consumer(sib_host, sib_port, cfp, "Room_" + roomno);

		// join smart space
		consumer.join();

		// start daemon
		consumer.run();
	}
}
