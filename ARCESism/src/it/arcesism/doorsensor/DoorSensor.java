package it.arcesism.doorsensor;

public interface DoorSensor {

    public boolean getDoorSlam();
    public void calibrate();
}
