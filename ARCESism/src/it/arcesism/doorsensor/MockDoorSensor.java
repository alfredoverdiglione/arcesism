package it.arcesism.doorsensor;

import java.util.Random;

public class MockDoorSensor implements  DoorSensor{
    // a simple mock

    private Random r;

    public MockDoorSensor(){
        r = new Random();
    }

    public boolean getDoorSlam() {
        return r.nextBoolean();
    }

    public void calibrate() {
        //do nothing
    }
}
