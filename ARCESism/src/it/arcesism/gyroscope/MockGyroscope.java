package it.arcesism.gyroscope;

import java.util.Random;

public class MockGyroscope implements Gyroscope {

    private Random r;

    /**Sezione riservata all'accelerazione*/
    public float getXAcceleration(){
        return r.nextFloat()*20;
    }
    public float getYAcceleration(){
        return r.nextFloat()*20;
    }
    public float getZAcceleration(){
        return r.nextFloat()*20;
    }

    /**Sezione riservata alla rotazione*/
    public float getXGyroscope(){
        return r.nextFloat()*20;
    }
    public float getYGyroscope(){
        return r.nextFloat()*20;
    }
    public float getZGyroscope(){
        return r.nextFloat()*20;
    }

    /**Calibration procedure*/
    public void calibrate() {
        r = new Random();
    }
}
