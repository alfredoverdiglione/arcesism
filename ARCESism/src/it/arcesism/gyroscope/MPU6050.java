/**@Author : Alfredo Verdiglione
 * @Description : In questo modulo ci preoccupiamo di come repererire le informazioni per noi interessanti da parte del giroscopio.
 * Tale giroscopio in questione si presenta come un dispositivo che fornisce le informazioni tramite la lettura
 * attraverso protocollo I2C dei suoi registri interni, per questo motivo questa classe,facendo uso della libreria
 * Pi4J legge i vari byte che sono a noi utili e li fornisce all'esterno per essere elaborati successivamente.
 * Prima di poter utilizzare la lettura occorre che il sensore venga fatto uscire dalla sleep mode in cui e'
 * per convenzione attraverso il comando di setup.
 * 
 * 
 * (Vogliamo usarlo come oggetto statico o metto una factory? Vado per la seconda.)
 * */


package it.arcesism.gyroscope;
/**Occhio che dovete avere il JAR che ho messo nel BuildPath altrimenti non 
 * fate niente.*/
import com.pi4j.wiringpi.I2C;


public class MPU6050 implements Gyroscope {
/*---------------------------------Sezione delle definizioni statiche---------------------------------*/
	public static final int deviceId = 0x68;
	public static final int ACCEL_XOUT = 0x3B;
	public static final int ACCEL_YOUT = 0x3D;
	public static final int ACCEL_ZOUT = 0x3F;
	public static final int GYRO_XOUT = 0x43;
	public static final int GYRO_YOUT = 0x45;
	public static final int GYRO_ZOUT = 0x47;
	public static final float GYRO_SENSITIVTY = 131.0f;
	public static final float ACCEL_SENSITIVY = 16384.0f;
/*----------------------------------------------------------------------------------------------------*/	
	private boolean sleep;
	private int fileDescriptor;
	//primo indice per colonne, secondo per righe;
	private float[][] rotationMatrix;

	private MPU6050(){
		
		sleep=true;
		fileDescriptor=I2C.wiringPiI2CSetup(deviceId);
		if(fileDescriptor<0) 
			throw new IllegalArgumentException("Can't initialize device...");
	}
	
	
	public boolean wakeUp(){
		I2C.wiringPiI2CWriteReg8(fileDescriptor, 0x6B, 0x0);
		sleep=false;
		return sleep;		
	}
	
	
	/**Accelerazione espressa in g come esce dal vettore*/
	public float getRawXAcceleration() {
		int H_BYTE = I2C.wiringPiI2CReadReg8(fileDescriptor, ACCEL_XOUT);
		int L_BYTE = I2C.wiringPiI2CReadReg8(fileDescriptor, ACCEL_XOUT+1);
		
		int RAW_VALUE = checkValue(((H_BYTE << 8 )| L_BYTE));
		
		return RAW_VALUE / ACCEL_SENSITIVY;
	}


	public float getRawYAcceleration() {
		int H_BYTE = I2C.wiringPiI2CReadReg8(fileDescriptor, ACCEL_YOUT);
		int L_BYTE = I2C.wiringPiI2CReadReg8(fileDescriptor, ACCEL_YOUT+1);
		
		int RAW_VALUE = checkValue(((H_BYTE << 8 )| L_BYTE));
		
		return RAW_VALUE / ACCEL_SENSITIVY;
	}


	public float getRawZAcceleration() {
		int H_BYTE = I2C.wiringPiI2CReadReg8(fileDescriptor, ACCEL_ZOUT);
		int L_BYTE = I2C.wiringPiI2CReadReg8(fileDescriptor, ACCEL_ZOUT+1);
		
		int RAW_VALUE = checkValue(((H_BYTE << 8 )| L_BYTE));
		
		return RAW_VALUE / ACCEL_SENSITIVY;
	}


	
	/**La misura avviene in gradi/secondo */
	
	public float getXGyroscope() {
		int H_BYTE = I2C.wiringPiI2CReadReg8(fileDescriptor, GYRO_XOUT);
		int L_BYTE = I2C.wiringPiI2CReadReg8(fileDescriptor, GYRO_XOUT+1);
		
		int RAW_VALUE = checkValue(((H_BYTE << 8 )| L_BYTE));
		return RAW_VALUE / GYRO_SENSITIVTY;
	}


	public float getYGyroscope() {
		int H_BYTE = I2C.wiringPiI2CReadReg8(fileDescriptor, GYRO_YOUT);
		int L_BYTE = I2C.wiringPiI2CReadReg8(fileDescriptor, GYRO_YOUT+1);
		
		int RAW_VALUE = checkValue(((H_BYTE << 8 )| L_BYTE));
		return RAW_VALUE / GYRO_SENSITIVTY;
	}


	public float getZGyroscope() {
		int H_BYTE = I2C.wiringPiI2CReadReg8(fileDescriptor, GYRO_ZOUT);
		int L_BYTE = I2C.wiringPiI2CReadReg8(fileDescriptor, GYRO_ZOUT+1);
		
		int RAW_VALUE = checkValue(((H_BYTE << 8 )| L_BYTE));
		return RAW_VALUE / GYRO_SENSITIVTY;
	}

	/** accelerazione calibrata */
	public void calibrate() {
		float rpx = getRawXAcceleration();
		float rpy = getRawYAcceleration();
		float rpz = getRawZAcceleration();
		float norm = (float) Math.sqrt(rpx*rpx + rpy*rpy + rpz*rpz);
		float px = rpx / norm;
		float py = rpy / norm;
		float pz = rpz / norm;
		float[] x = new float[]{ 1 / norm, -px/py/norm, 0};
		float[] y = new float[]{ -px/norm, -py/norm, -pz/norm};
		float[] z = new float[]{ x[1]*y[2]/norm - y[1]*x[2]/norm,
								 y[0]*x[2]/norm - x[0]*y[2]/norm,
								 x[0]*y[1]/norm - y[0]*x[1]/norm};
		rotationMatrix = new float[][] { x, y, z };
		System.out.println("Calibrated for x " + rpx + " y " + rpy + " z " + rpz + "\nMatrix:\n");
		System.out.println("Normalized x " + px + " y " + py + " z " + pz + "\nMatrix:\n");
		System.out.println(rotationMatrix[0][0] + "\t" + rotationMatrix[1][0] + "\t" + rotationMatrix[2][0]);
		System.out.println(rotationMatrix[0][1] + "\t" + rotationMatrix[1][1] + "\t" + rotationMatrix[2][1]);
		System.out.println(rotationMatrix[0][2] + "\t" + rotationMatrix[1][2] + "\t" + rotationMatrix[2][2]);
		System.out.println("Result: x " + (rpx * rotationMatrix[0][0]
				+ rpy * rotationMatrix[0][1]
				+ rpz * rotationMatrix[0][2]) + " y " + (rpx * rotationMatrix[1][0]
				+ rpy * rotationMatrix[1][1]
				+ rpz * rotationMatrix[1][2]) + " z " + (rpx * rotationMatrix[2][0]
				+ rpy * rotationMatrix[2][1]
				+ rpz * rotationMatrix[2][2]));
	}

	public float getXAcceleration() {
		if (rotationMatrix == null)
			throw new IllegalArgumentException("sensor not calibrated");
		float rpx = getRawXAcceleration();
		float rpy = getRawYAcceleration();
		float rpz = getRawZAcceleration();
		return rpx * rotationMatrix[0][0]
				+ rpy * rotationMatrix[0][1]
				+ rpz * rotationMatrix[0][2];
	}

	public float getYAcceleration() {
		if (rotationMatrix == null)
			throw new IllegalArgumentException("sensor not calibrated");
		float rpx = getRawXAcceleration();
		float rpy = getRawYAcceleration();
		float rpz = getRawZAcceleration();
		return rpx * rotationMatrix[1][0]
				+ rpy * rotationMatrix[1][1]
				+ rpz * rotationMatrix[1][2];
	}

	public float getZAcceleration() {
		if (rotationMatrix == null)
			throw new IllegalArgumentException("sensor not calibrated");
		float rpx = getRawXAcceleration();
		float rpy = getRawYAcceleration();
		float rpz = getRawZAcceleration();
		return rpx * rotationMatrix[2][0]
				+ rpy * rotationMatrix[2][1]
				+ rpz * rotationMatrix[2][2];
	}

	/**Sezione dei comandi static quali la factory*/
	
	public static MPU6050 GetInstance(){
		return new MPU6050();		
	}
		
	private static int checkValue(int in){
		if(in>= 0x8000)
			return -((65535-in)+1);
		return in;
		
	}
}
