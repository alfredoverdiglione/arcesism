package it.arcesism.gyroscope;

public interface Gyroscope {
	
	/**Sezione riservata all'accelerazione*/
	public float getXAcceleration();
	public float getYAcceleration();
	public float getZAcceleration();
	
	/**Sezione riservata alla rotazione*/
	public float getXGyroscope();
	public float getYGyroscope();
	public float getZGyroscope();

	/**Calibration procedure*/
	public void calibrate();

	/**Per un qualche strano motivo il sensore legge anche la temperatura,
	 * lo vogliamo includere?*/
	
	
}
