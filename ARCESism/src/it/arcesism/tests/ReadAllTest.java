package it.arcesism.tests;

import java.io.IOException;

import it.arcesism.gyroscope.MPU6050;

public class ReadAllTest {

	public static void main(String[] args) {
		boolean raw = false;
		boolean burst = true;
		if (args.length >= 1 && args[0] == "-r"){
			raw = true;
			System.out.println("RAW MODE");
		}
		if (args.length >= 1 && args[0] == "-b"){
			burst = true;
			System.out.println("BURST MODE");
		}
		
		MPU6050 testSensor = MPU6050.GetInstance();
		System.out.println(
					testSensor.wakeUp()?
							"Sensore non attivato!":
							"Sensore correttamente attivato!");
		if (!raw)
			testSensor.calibrate();	
		while(true){
			if (!raw && !burst){
			System.out.println("--------Map of value-----------");
			System.out.println("Accel-X: \t"+testSensor.getXAcceleration());
			System.out.println("Accel-Y: \t"+testSensor.getYAcceleration());
			System.out.println("Accel-Z: \t"+testSensor.getZAcceleration());
			System.out.println("Gyro-X: \t"+testSensor.getXGyroscope());
			System.out.println("Gyro-Y: \t"+testSensor.getXGyroscope());
			System.out.println("Gyro-Z: \t"+testSensor.getXGyroscope());}
			else if (!burst){
				System.out.println("--------Map of value-----------");
				System.out.println("Accel-X: \t"+testSensor.getRawXAcceleration());
				System.out.println("Accel-Y: \t"+testSensor.getRawYAcceleration());
				System.out.println("Accel-Z: \t"+testSensor.getRawZAcceleration());
				System.out.println("Gyro-X: \t"+testSensor.getXGyroscope());
				System.out.println("Gyro-Y: \t"+testSensor.getXGyroscope());
				System.out.println("Gyro-Z: \t"+testSensor.getXGyroscope());
			} else {
				System.out.print("Accel-X: \t"+testSensor.getXAcceleration());
				System.out.print("Accel-Y: \t"+testSensor.getYAcceleration());
				System.out.println("Accel-Z: \t"+testSensor.getZAcceleration());
			}
			try {
				Process process = Runtime.getRuntime().exec("clear");
				process.waitFor();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			try {
				Thread.sleep(burst? 50 : 1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
