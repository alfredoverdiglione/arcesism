package it.arcesism.initializer;

import java.util.Vector;

import it.arcesism.Constants;
import sofia_kp.KPICore;
import sofia_kp.SIBResponse;

public class Initializer {
	public static final String usage = "Initializer - usage:\n"
			+ "java -jar this_jar sib_host sib_port how_many_quake_sensors how_many_door_sensors\n"
			+ "or just java -jar this_jar -a to autoconfig it for local operation, single room, single quake sensor, single door sensor\n"
			+ "multiple room configuration not available (for now),";
	static int SIBPort = 10010;
	static String SIB_host = "127.0.0.1";
	static int rooms = 1, qSensors = 1, dSensors = 1;
	static String SIBName = "X";
	KPICore kp;
	SIBResponse resp = new SIBResponse();
	Vector<Vector<String>> literalTriples = new Vector<Vector<String>>();
	Vector<Vector<String>> ObjectTriples = new Vector<Vector<String>>();

	public static void main(String[] args) {
		boolean solved = false;

		try {
			if (args.length == 1 && args[0].equals("-a")) {
				solved = true;
			} else if (args.length == 4) {
				SIB_host = args[0];
				SIBPort = Integer.parseInt(args[1]);
				qSensors = Integer.parseInt(args[2]);
				dSensors = Integer.parseInt(args[3]);
				solved = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (!solved) {
				System.out.println(usage);
				System.exit(99);
			}
		}

		System.out.println("Initialising ARCESism");
		
		System.out.println("\n---CLEANING  SIB---");
		KPICore kp;
		SIBResponse resp = null;
		kp = new KPICore(SIB_host, SIBPort, SIBName);
        resp = kp.join();
        if(!resp.isConfirmed()){
        	System.err.println("Error joining the SIB");
        	System.exit(0);
        }else{
        	System.out.println("Joined!");
        }
		resp = kp.remove("http://www.nokia.com/NRC/M3/sib#any", "http://www.nokia.com/NRC/M3/sib#any", "http://www.nokia.com/NRC/M3/sib#any",
				"uri", "uri");
		System.out.println("-------------------");
		
		System.out.println("\n----ROOM  SETUP----");
		for (int i = 1; i <= rooms; i++){
			System.out.println("Adding Room " + i);
			kp.insert(Constants.ns + "Room_" + i, "rdf:type", "Room", "uri", "literal");
			kp.insert(Constants.ns + "Room_" + i, Constants.ns + "HasFriendlyName", "Room " + i, "uri", "literal");
		}
		System.out.println("-------------------");
		
		System.out.println("\n---SENSOR  SETUP---");
		// Quake Sensors
		for (int i = 1; i <= qSensors; i++){
			System.out.println("Adding Quake Sensor " + i + " in Room 1");
			kp.insert(Constants.ns + "QuakeSensor_" + i, "rdf:type", "QuakeSensor", "uri", "literal");
			kp.insert(Constants.ns + "QuakeSensor_" + i, Constants.ns + "IsInRoom", Constants.ns + "Room_1", "uri", "uri");
		}
		// Door Sensors
		for (int i = 1; i <= dSensors; i++){
			System.out.println("Adding Door Sensor " + i + " in Room 1");
			kp.insert(Constants.ns + "DoorSensor_" + i, "rdf:type", "DoorSensor", "uri", "literal");
			kp.insert(Constants.ns + "DoorSensor_" + i, Constants.ns + "IsInRoom", Constants.ns + "Room_1", "uri", "uri");
		}
		System.out.println("-------------------");
		
		System.out.println("\n--MEASURES  SETUP--");
		//UOMs
		System.out.println("Configuring UoMs");
		kp.insert(Constants.ns + "Meters_per_Second_Squared", "rdf:type", "UoM", "uri", "literal");
		
		//Measurements
		System.out.println("Configuring Measurements");
		kp.insert(Constants.ns + "Peak_Ground_Acceleration", "rdf:type", "Measurand", "uri", "literal");
		kp.insert(Constants.ns + "Door_Slammed", "rdf:type", "Measurand", "uri", "literal");
		System.out.println("-------------------");
		
		kp.leave();
		System.out.println("\n---INIT COMPLETE---");
	}


}
