# Ontologia

## Contenuti della Cartella

+ *ARCES.rdf*: ontologia realizzata in Protege.
+ *Architettura Software.pptx*: schema di interazione dei diversi KP con la SIB.
+ *Schema Interazione.pptx*: schema che rappresenta un esempio di dati esistenti sulla SIB in un dato istante di tempo, con le loro correlazioni.
+ *Ontologia Java.mdj*: file di StarUML con il diagramma delle classi del progetto Java.
+ *Ontologia Java.pdf*: esportazione di *Ontologia Java.mdj* per fruizione pubblica.
